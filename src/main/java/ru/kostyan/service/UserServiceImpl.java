package ru.kostyan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.kostyan.model.User;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private String BASE_URL = "http://localhost:8081/admin/rest/";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void add(User user) {
        HttpEntity<User> request = new HttpEntity<>(user);
        ResponseEntity<User> response =
                restTemplate.exchange(BASE_URL,
                        HttpMethod.POST,
                        request, User.class);
    }

    @Override
    public void delete(Long id) {
        System.out.println(id);
        restTemplate.delete(BASE_URL + id);


    }

    @Override
    public void edit(User user) {
        HttpEntity<User> request = new HttpEntity<>(user);
        ResponseEntity<User> response =
                restTemplate.exchange(BASE_URL,
                        HttpMethod.PUT,
                        request, User.class);
    }

    @Override
    public User getById(Long id) {
        ResponseEntity<User> response = restTemplate.getForEntity(BASE_URL + id, User.class);
        return response.getBody();
    }

    @Override
    public List<User> getAllUsers() {
        ResponseEntity<List<User>> rateResponse =
                restTemplate.exchange(BASE_URL + "users",
                        HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<User>>(){});
        return rateResponse.getBody();
    }
}
