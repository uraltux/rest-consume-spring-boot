package ru.kostyan.consumerest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ComponentScan("ru.kostyan")
public class ConsumeRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumeRestApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.additionalInterceptors(new BasicAuthenticationInterceptor("uraltux@gmail.com", "1")).build();
    }


}
