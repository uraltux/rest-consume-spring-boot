url1 = "http://localhost:8082/api/";

function getAllUser() {
    fetch(url1 + 'users').then(function (response) {
        response.json().then(function (data) {
            let tr;
            console.log(data);
            $('#tableJquery tbody').empty();
            for (var i = 0; i < data.length; i++) {
                tr = $('<tr/>');
                tr.append("<td>" + data[i].id + "</td>");
                tr.append("<td>" + data[i].roles + "</td>");
                tr.append("<td>" + data[i].name + "</td>");
                tr.append("<td>" + data[i].email + "</td>");
                tr.append("<td>" + data[i].password + "</td>");
                tr.append("<td>" + data[i].active + "</td>");
                tr.append("<td>" + "<button type='button' onclick='btnEditUserAction(" + data[i].id + ")' class='btn btn-primary btn-sm' id='bntEditUser' \>Edit</button><button type='button' onclick='btnDeleteUserAction(" + data[i].id + ")' id='btnDeleteUser' class='btn btn-danger btn-sm'>Delete</button>" + "</td>");
                $('#tableJquery').append(tr);
            }
        })
    });
}
getAllUser();

function btnDeleteUserAction(userId) {
    $.ajax({
        url: url1 + userId,
        method: 'DELETE',
        success: function () {
            getAllUser();
            $('#successDelete').show('slow');
            setTimeout(function () {
                $('#successDelete').hide('slow')},1500);
        },
        error: function (error) {
            alert(error);
        }
    })
}

function btnAddUser() {
    const roles=[];
    if ($('#newUser-role-admin').is(":checked")) {
        roles.push('ADMIN')
    }
    if ($('#newUser-role-user').is(":checked")) {
        roles.push('USER');
    }
    const user = {
        name: document.getElementById('newUserName').value,
        email: document.getElementById('InputEmail').value,
        password: document.getElementById('InputPassword').value,
        roles: roles,
        active: $('#newUserActive').is(":checked")
    };
    fetch(url1,{
        method:'POST',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json'
        },
        body:JSON.stringify(user),
    });
    location.reload();

}

function btnEditUserAction(userId) {
    $('#ModalCenter').addClass('show d-block');
    $('#user-role-admin').prop('checked', false);
    $('#user-role-user').prop('checked', false);
    fetch(url1 + userId).then(function (response) {
        response.json().then(function (data) {
            console.log('data', data);
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#email').val(data.email);
            $('#password').val(data.password);
            $('#user-active').prop('checked', data.active);
            if ($.inArray("ADMIN", data.roles) != -1) {
                $('#user-role-admin').prop('checked', true);
            }
            if ($.inArray("USER", data.roles) != -1) {
                $('#user-role-user').prop('checked', true);
            }
        })
    });
}

function btnCloseModal() {
    $('#ModalCenter').removeClass('show d-block');

}

function putForm() {
    const roles=[];
    if ($('#user-role-admin').is(":checked")) {
        roles.push('ADMIN')
    }
    if ($('#user-role-user').is(":checked")) {
        roles.push('USER');
    }
    const user = {
        id: document.getElementById('id').value,
        name: document.getElementById('name').value,
        email: document.getElementById('email').value,
        password: document.getElementById('password').value,
        roles: roles,
        active: $('#user-active').is(":checked")
    };

    fetch(url1,{
        method:'PUT',
        headers:{
            'Accept':'application/json',
            'Content-Type':'application/json'
        },
        body:JSON.stringify(user),
    });
    $.ajax({
            success: function () {
            getAllUser();
            btnCloseModal();
                    },
        error: function (error) {
            alert(error);
        }
    })
}
